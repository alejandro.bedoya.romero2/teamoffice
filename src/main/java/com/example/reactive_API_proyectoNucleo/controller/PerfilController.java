package com.example.reactive_API_proyectoNucleo.controller;

import com.example.reactive_API_proyectoNucleo.model.Perfil;
import com.example.reactive_API_proyectoNucleo.repository.perfilRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/perfil")
public class PerfilController {
    @Autowired
    perfilRepository repository;

    @GetMapping
    /*tipos de flujos Flux and mono*/
    public Flux<Perfil> getPerfils(){
        return repository.findAll();
    }
    @GetMapping("/{id}")
    public Mono<Perfil> getPerfil(@PathVariable Integer id){
        return repository.findById(id);
    }


}
