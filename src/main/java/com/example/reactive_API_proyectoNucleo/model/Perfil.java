package com.example.reactive_API_proyectoNucleo.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Table
@Data
public class Perfil {
    @Id
    private Integer id;
    @Column
    private  String rol;
    @Column
    private  String observacion;
}
