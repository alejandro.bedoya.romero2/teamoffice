package com.example.reactive_API_proyectoNucleo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReactiveApiProyectoNucleoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReactiveApiProyectoNucleoApplication.class, args);
	}

}
