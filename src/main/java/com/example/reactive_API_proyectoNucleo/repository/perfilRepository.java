package com.example.reactive_API_proyectoNucleo.repository;

import com.example.reactive_API_proyectoNucleo.model.Perfil;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface perfilRepository extends ReactiveCrudRepository<Perfil, Integer>{
}
